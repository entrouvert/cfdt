<?php

class cfdtRegion extends LSldapObject {
  var $userObjectType = 'cfdtUtilisateur';
  var $memberAttr = 'member';
  var $memberAttrValue = 'dn';

  /* ========== Members ========== */
  /**
   * Retourne la valeur clef d'un membre
   * 
   * @param[in] $object Un object utilisateur
   * 
   * @retval Mixed La valeur clef d'un membre
   **/
  function getMemberKeyValue($object) {
    return $this -> getObjectKeyValueInRelation($object,$this -> memberAttr,$this -> userObjectType);
  }
  
  /**
   * Retourne la liste des groupes pour utilisateur
   * 
   * Retourne un tableau de LSsupannGroup correspondant aux groupes
   * auxquels appartient un utilisateur
   * 
   * @param[in] $userObject Un object user (type : $this -> userObjectType)
   * 
   * @retval Array of LSsupannGroup Les groupes de l'utilisateur
   **/
  function listUserGroups($userObject) {
    return $this -> listObjectsInRelation($userObject,$this -> memberAttr,$this -> userObjectType,$this -> memberAttrValue);
  }

  /**
   * Ajoute un utilisateur au groupe
   * 
   * @param[in] $object Un object user ($this -> userObjectType) : l'utilisateur à ajouter
   * 
   * @retval boolean true si l'utilisateur à été ajouté, False sinon
   **/  
  function addOneMember($object) {
    return $this -> addOneObjectInRelation($object,$this -> memberAttr, $this -> userObjectType,$this -> memberAttrValue,'canEditGroupRelation');
  }
  
  /**
   * Supprime un utilisateur du groupe
   * 
   * @param[in] $object Un object (type : $this -> userObjectType) : l'utilisateur à supprimer
   * 
   * @retval boolean true si l'utilisateur à été supprimé, False sinon
   **/  
  function deleteOneMember($object) {
    return $this -> deleteOneObjectInRelation($object,$this -> memberAttr,$this -> userObjectType,$this -> memberAttrValue,'canEditGroupRelation');
  }
  
 /**
  * Renome un utilisateur du groupe
  * 
  * @param[in] $object Un object (type : $this -> userObjectType) : l'utilisateur à renomer
  * @param[in] $oldDn string L'ancien DN de l'utilisateur
  * 
  * @retval boolean True en cas de succès, False sinon
  */
  function renameOneMember($object,$oldDn) {
    return $this -> renameOneObjectInRelation($object,$oldDn,$this -> memberAttr,$this -> userObjectType,$this -> memberAttrValue);
  }
  
  /**
   * Met à jour les groupes d'un utilisateur
   * 
   * @param[in] $object Mixed Un object (type : $this -> userObjectType) : l'utilisateur
   * @param[in] $listDns Array(string) Un tableau des DNs des groupes de l'utilisateur
   * 
   * @retval boolean true si tout c'est bien passé, False sinon
   **/  
  function updateUserGroups($object,$listDns) {
    return $this -> updateObjectsInRelation($object,$listDns,$this -> memberAttr,$this -> userObjectType,$this -> memberAttrValue,'canEditGroupRelation');
  }

  /**
   * Test si l'utilisateur peut d'editer la relation avec ce groupe
   * 
   * @retval boolean true si tout l'utilisateur peut éditer la relation, False sinon
   **/  
  function canEditGroupRelation($dn=NULL) {
    if (!$dn) {
      $dn=$this -> dn;
    }
    return LSsession :: canEdit($this -> type_name,$this -> dn,$this -> memberAttr);
  }
}

?>
