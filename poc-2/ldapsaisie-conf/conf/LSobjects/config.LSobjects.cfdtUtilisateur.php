<?php

$GLOBALS['LSobjects']['cfdtUtilisateur'] = array (
  'objectclass' => array(
    'cfdtUtilisateur'
  ),
  'rdn' => 'uid',
  'container_dn' => 'ou=personnes',
  'label' => 'Utilisateur',
  'display_name_format' => '%{givenName} %{sn}',
  'displayAttrName' => true,
  // LSform
  'LSform' => array (
    'layout' => array(
      'user' => array(
        'label' => 'Utilisateur',
        'args' => array(
          'uid',
	  'givenName',
	  'sn',
	  'cfdtNPA',
	  'mail',
	  'cfdtEmail',
          'userPassword',
          'cfdtIdentifiantCompteMDM',
	  'cfdtUserPasswordHistorique',
	  'description',
	  'cfdtDateDeCreation',
	  'cfdtDateDActivation',
	  'cfdtUserStatus',
        )
      )
    )
  ), // fin LSform

  // LSrelation
  'LSrelation' => array(
    'roles' => array(
      'label' => 'Est membre des rôles...',
      'emptyText' => "N'est membre d'aucun rôle",
      'LSobject' => 'cfdtRole',
      'list_function' => 'listUserGroups',
      'getkeyvalue_function' => 'getMemberKeyValue',
      'update_function' => 'updateUserGroups',
      'remove_function' => 'deleteOneMember',
      'rename_function' => 'renameOneMember',
      'canEdit_function' => 'canEditGroupRelation',
      'canEdit_attribute' => 'member',
      'rights' => array(
        'self' => 'w',
        'admin' => 'w',
      )
    ),
    'federations' => array(
      'label' => 'Est membre des fédérations...',
      'emptyText' => "N'est membre d'aucune fédération",
      'LSobject' => 'cfdtFederation',
      'list_function' => 'listUserGroups',
      'getkeyvalue_function' => 'getMemberKeyValue',
      'update_function' => 'updateUserGroups',
      'remove_function' => 'deleteOneMember',
      'rename_function' => 'renameOneMember',
      'canEdit_function' => 'canEditGroupRelation',
      'canEdit_attribute' => 'member',
      'rights' => array(
        'self' => 'w',
        'admin' => 'w',
      )
    ),
    'regions' => array(
      'label' => 'Est membre des régions...',
      'emptyText' => "N'est membre d'aucune région",
      'LSobject' => 'cfdtRegion',
      'list_function' => 'listUserGroups',
      'getkeyvalue_function' => 'getMemberKeyValue',
      'update_function' => 'updateUserGroups',
      'remove_function' => 'deleteOneMember',
      'rename_function' => 'renameOneMember',
      'canEdit_function' => 'canEditGroupRelation',
      'canEdit_attribute' => 'member',
      'rights' => array(
        'self' => 'w',
        'admin' => 'w',
      )
    ),
  ),


  // LSsearch
  'LSsearch' => array (
    'attrs' => array(
      'givenName',
      'sn',
      'uid',
      'mail',
      'cfdtEmail',
      'cfdtNPA',
      'cfdtIdentifiantCompteMDM'
    )
  ), // fin LSsearch
  // Attributs
  'attrs' => array (
    /* ----------- start -----------*/
    'uid' => array (
      'label' => 'Identifiant',
      'help_info' => 'L\'identifiant ne doit contenir que des lettres , des chiffres, des tirets, des underscores ou des points.',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'check_data' => array (
        'regex' => array(
          'msg' => "L'identifiant ne doit contenir que des lettres , des chiffres, des tirets, des underscores ou des points.",
          'params' => array(
            'regex' => '/^[-_.a-zA-Z0-9]+$/',
          ),
        ),
      ),
      'validation' => array (
        array (
          'filter' => 'uid=%{val}',
          'result' => 0,
          'msg' => 'Cet identifiant est déjà utilisé.'
        )
      ),
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'userPassword' => array (
      'label' => 'Mot de passe',
      'help_info' => 'Le mot de passe doit faire plus de 8 caractères.',
      'ldap_type' => 'password',
      'html_type' => 'password',
      'html_options' => array(
        'generationTool' => true,
        'autoGenerate' => false,
        'lenght' => 8,
        'chars' => array (
          array (
            'chars' => '23456789abcdefjkpqrstwxyzABCDEFJKPQRSTWXYZ',
            'nb' => 6
          )
        ),
      ),
      'check_data' => array(
        'password' => array(
          'msg' => 'Le mot de passe doit faire plus de 8 caractères.',
          'params' => array(
            'minLength' => 8,
          )
        )
      ),
      'required' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1,
        'lostPassword' => 1
      ),
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'givenName' => array (
      'label' => 'Prénom',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'rights' => array(
        'self' => 'w',
        'admin' => 'w',
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'sn' => array (
      'label' => 'Nom',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'cfdtNPA' => array (
      'label' => 'Identifiant CFDT',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'mail' => array (
      'label' => 'E-mail',
      'ldap_type' => 'ascii',
      'html_type' => 'mail',
      'check_data' => array (
        'email' => array(
          'msg' => "Cette adresse est invalide.",
        ),
      ),
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'cfdtEmail' => array (
      'label' => 'E-mail CFDT',
      'ldap_type' => 'ascii',
      'html_type' => 'mail',
      'check_data' => array (
        'email' => array(
          'msg' => "Cette adresse est invalide.",
        ),
      ),
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'cfdtIdentifiantCompteMDM' => array (
      'label' => 'Identifiant MDM',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'description' => array (
      'label' => 'Description/Commentaire',
      'ldap_type' => 'ascii',
      'html_type' => 'textarea',
      'multiple' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'cfdtDateDeCreation' => array (
      'label' => 'Date de création',
      'ldap_type' => 'date',
      'html_type' => 'date',
      'multiple' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'cfdtDateDActivation' => array (
      'label' => 'Date d\'activation',
      'ldap_type' => 'date',
      'html_type' => 'date',
      'multiple' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'cfdtUserStatus' => array (
      'label' => 'Statut',
      'ldap_type' => 'ascii',
      'html_type' => 'select_box',
      'html_options' => array(
         'possibles_values' => array(
            'statut1',
            'statut2',
            'statut3',
         )
      ),
      'multiple' => 1,
      'rights' => array(
        'self' => 'w',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
  )
);
