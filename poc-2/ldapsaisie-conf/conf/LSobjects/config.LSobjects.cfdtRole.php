<?php

$GLOBALS['LSobjects']['cfdtRole'] = array (
  'objectclass' => array(
    'cfdtRole'
  ),
  'rdn' => 'cn',
  'container_dn' => 'ou=roles,ou=groupes',
  'label' => 'Rôle',
  'display_name_format' => '%{cn}',
  'displayAttrName' => true,

  'LSform' => array (
    'layout' => array(
      'role' => array(
        'label' => 'Rôle',
        'args' => array(
          'cn',
          'description',
          'member',
        )
      )
    )
  ), // fin LSform

  // LSsearch
  'LSsearch' => array (
    'attrs' => array(
       'cn',
    )
  ), // fin LSsearch

  'LSrelation' => array(
     'applications' => array(
       'label' => 'A accès aux applications...',
       'emptyText' => "N'a accés à aucune application",
       'LSobject' => 'cfdtApplication',
       'list_function' => 'listUserGroups',
       'getkeyvalue_function' => 'getMemberKeyValue',
       'update_function' => 'updateUserGroups',
       'remove_function' => 'deleteOneMember',
       'rename_function' => 'renameOneMember',
       'canEdit_function' => 'canEditGroupRelation',
       'canEdit_attribute' => 'member',
       'rights' => array(
         'self' => 'r',
         'admin' => 'w',
       )
     ),
  ),

  // Attributs
  'attrs' => array (
    /* ----------- start -----------*/
    'cn' => array (
      'label' => 'Identifiant',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'validation' => array (
        array (
          'filter' => 'cn=%{val}',
          'result' => 0,
          'msg' => 'Cet identifiant est déjà utilisé.'
        )
      ),
      'rights' => array(
        'self' => 'r',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'description' => array (
      'label' => 'Description/Commentaire',
      'ldap_type' => 'ascii',
      'html_type' => 'textarea',
      'required' => 1,
      'multiple' => 1,
      'rights' => array(
        'self' => 'r',
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/
    /* ----------- start -----------*/
    'member' => array (
      'label' => 'Membres',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'cfdtUtilisateur',                      // Nom de l'objet à lister
          'display_name_format' => '%{givenName} %{sn} (%{uid})',      // Spécifie le attributs à lister pour le choix,
                                                              // si non définie => utilisation du 'display_name_format'
                                                              // de la définition de l'objet

          'value_attribute' => 'dn',                          // Spécifie le attributs dont la valeur sera retournée par
        )
      ),
      'required' => 1,
      'multiple' => 1,
      'validation' => array (
        array (
          'basedn' => '%{val}',
          'result' => 1
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w',
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

  )
);

