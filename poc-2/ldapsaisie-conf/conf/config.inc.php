<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

// Configuration LDAP Saisie :
$GLOBALS['LSconfig'] = array(
  'NetLDAP2' => '/usr/share/php/Net/LDAP2.php',
  'Smarty' => '/usr/share/php/smarty3/Smarty.class.php',
  'lang' => 'fr_FR',
  'encoding' => 'UTF8',
  'cacheLSprofiles' => true,
  'cacheSubDn' => true,
  'cacheSearch' => false,
  'keepLSsessionActive' => true,
  'ldap_servers' => array (
    array (
      'name' => 'LSexample',
      'ldap_config'=> array(
            'host'     => '127.0.0.1',
            'port'     => 389,
            'version'  => 3,
            'starttls' => false,
            'binddn'   => 'cn=admin,dc=cfdt,dc=fr',
            'bindpw'   => 'admin',
            'basedn'   => 'dc=cfdt,dc=fr',
            'options'  => array(),
            'filter'   => '(objectClass=*)',
            'scope'    => 'sub'
      ),
      'LSprofiles' => array (
        'admin' => array(
          'dc=cfdt,dc=fr' => 'uid=admin,ou=personnes,dc=cfdt,dc=fr',
        )
      ),
      'cacheLSprofiles' => true,
      'cacheSearch' => true,
      'authObjectType' => 'cfdtUtilisateur',
      'authObjectFilter' => '(|(uid=%{user})(cfdtEmail=%{user})(mail=%{user}))',
      'authObjectTypeAttrPwd' => 'userPassword',
      'LSaccess' => array(
        'cfdtUtilisateur',
        'cfdtRole',
        'cfdtApplication',
        'cfdtFederation',
        'cfdtRegion',
      ),
      'emailSender' => 'noreply@cfdt.fr'
    ),
  )
);

// Interface
// Theme Black
//define('LS_THEME','black');
//define('LS_TEMPLATES_DIR', 'templates/default');

// Theme Default
define('LS_THEME','default');
define('LS_TEMPLATES_DIR', 'templates');
define('LS_IMAGES_DIR', 'images');
define('LS_CSS_DIR', 'css');

//Debug
define('LSdebug',false);

// Logs
$GLOBALS['LSlog']['filename'] = '/var/log/ldapsaisie/ldapsaisie.log';
$GLOBALS['LSlog']['enable'] = true;

define('NB_LSOBJECT_LIST',20);
define('NB_LSOBJECT_LIST_SELECT',11);

define('MAX_SEND_FILE_SIZE',2000000);


// Javascript
$GLOBALS['defaultJSscipts']=array(
  'mootools-core.js',
  'mootools-more.js',
  'functions.js',
  'LSdefault.js',
  'LSinfosBox.js'
);

// CSS
$GLOBALS['defaultCSSfiles']=array();
