#!/bin/bash

PATH=/sbin:/usr/sbin:$PATH

set -e

function try {
	LOG=`tempfile`
	if ! "$@" >$LOG 2>&1; then
		EXIT=$?
		cat $LOG
		exit $EXIT
	fi
}

CONFIG=`pwd`/data/slapd.d
DB=`pwd`/data/db

NEW=0
if [ ! -d data ]; then
	NEW=1
	mkdir -p data data/db data/slapd.d
	cat base.ldif >>data/config.ldif
	echo >>data/config.ldif
	for schema in core cosine inetorgperson nis cfdt; do
		cat schemas/${schema}.ldif >>data/config.ldif
		echo >>data/config.ldif
	done;
       	m4 -DPATH=$DB db.ldif >>data/config.ldif
	try slapadd -n0 -F $CONFIG -l data/config.ldif
fi

slapd -h "ldapi://data%2Fsocket/ ldap://localhost:1389/" -F $CONFIG -d768 >>data/log 2>&1 &
SLAPD_PID=$!
echo $SLAPD_PID >data/pid
echo Le serveur a le PID $SLAPD_PID
echo il est accessible via l\'URL ldap://localhost:1389
echo ou ldapi://data%2Fsocket
echo
echo identifiant admin: dc=cfdt,dc=fr
echo mot de passe: admin
echo
echo il existe des utilisateurs uid=userX,ou=personnes,dc=cfdt,dc=fr
echo leur mot de passe est \"test\"
echo
echo Exemples d\'interrogation
echo
echo 	ldapsearch -H ldapi://data%2Fsocket -Y EXTERNAL -b dc=cfdt,dc=fr
echo
echo 	ldapsearch -H ldap://localhost:1389 -D dc=cfdt,dc=fr -w admin -b dc=cfdt,dc=fr
echo
if [[ "$NEW" = "1" ]]; then
	sleep 1
	try ldapadd -H ldapi://data%2Fsocket -Y EXTERNAL -f <(m4 data.ldif)
fi
